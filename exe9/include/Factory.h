#ifndef FACTORY_H
#define FACTORY_H

#include<GUIFactory.h>
#include<WindowsButton.h>
#include<OSXButton.h>

class Factory: public GUIFactory
{
    public:
        Factory();
        Button *createButton(char * type);
        virtual ~Factory();

    protected:

    private:
};

#endif // FACTORY_H
