#ifndef GUIFACTORY_H
#define GUIFACTORY_H

#include<Button.h>

class GUIFactory
{
    public:
        GUIFactory();
        virtual Button *createButton(char *) = 0;
        virtual ~GUIFactory();

    protected:

    private:
};

#endif // GUIFACTORY_H
