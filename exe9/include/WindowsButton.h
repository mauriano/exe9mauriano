#ifndef WINDOWSBUTTON_H
#define WINDOWSBUTTON_H

#include<Button.h>

class WindowsButton: public Button
{
    public:
        WindowsButton();
        virtual ~WindowsButton();
        void paint();

    protected:

    private:
};

#endif // WINDOWSBUTTON_H
