#ifndef OSXBUTTON_H
#define OSXBUTTON_H

#include<Button.h>


class OSXButton: public Button
{
    public:
        OSXButton();
        virtual ~OSXButton();
        void paint();


    protected:

    private:
};

#endif // OSXBUTTON_H
