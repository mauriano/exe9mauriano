#include "Factory.h"

Factory::Factory()
{
    //ctor
}

Factory::~Factory()
{
    //dtor
}

Button *Factory::createButton(char * type){
    if(!strcmp(type, "Windows"))
        return new WindowsButton;
    else if(!strcmp(type, "OSX"))
        return new OSXButton;
}
