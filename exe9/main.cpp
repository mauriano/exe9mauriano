#include <iostream>

#include<GUIFactory.h>
#include<Factory.h>
#include<Button.h>


using namespace std;

int main()
{
    GUIFactory* guiFactory;
    Button* button;

    guiFactory = new Factory;

    button = guiFactory->createButton("OSX");
    button->paint();
    button = guiFactory->createButton("Windows");
    button->paint();

    return 0;
}
